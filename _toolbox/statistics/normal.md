---
layout: toolbox
sidebar:
    nav: "toolbox"
title: Normal Distribution
mathjax: true
---

{% assign eqn_count = 1 %}

The Gaussian distribution is likely the most-used statistical distribution. The distribution is definedusing two 
parameters: the distribution mean (μ) and the distribution standard distribution (σ). Another useful parameter is the 
distribution variance (σ<sup>2</sup>). The Gaussian is also known as the normal distribution. The general equation for 
a Gaussian distribution is expressed in the following equation:

$$ f(x) = \frac{1}{\sigma \sqrt{2 \pi}} e^{-\frac{1}{2} (\frac{x-\mu}{\sigma})^2} $$

The standard Gaussian distribution is a normalized distribution for which the integral from $-\infty$ to $+\infty$ 
gives a result of 1. It has a mean of 0 and a standard deviation of 1. See the following picture of a standard Gaussian 
distribution.

![Standard Gaussian distribution](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Standard_deviation_diagram.svg/350px-Standard_deviation_diagram.svg.png){: .align-center}

## Parameter Definitions

### Distribution Mean

The distribution mean is the average of all samples composing the distribution. The distribution mean is defined as:

$$ \mu = \frac{\sum_{k=1}^n x_k}{n} $$

where
- $k$ is the sample index
- $n$ is the number of samples
- $x_k$ is the $k_{th}$ sample

### Distribution Variance

The distribution variance is the average of the squared offsets between the samples and the distribution mean. The 
equation used to obtain the distribution variance will depend on if the distribution's data is the entire analyzed 
population or if it is only a fraction of it. This difference is minor but useful; a correction factor is introduced if 
the data is not the entire population. If all values of a population are used in these calculations, the distribution 
variance is defined as:

$$ \sigma^2 = \frac{\sum_{k=1}^n (x_k-\mu)^2}{n} $$

where
- $k$ is the sample index
- $n$ is the number of samples
- $x_k$ is the $k_{th}$ sample
- $\mu$ is the distribution mean

If only a few values of a larger population are used in these calculations, the distribution variance is defined as:

$$ \sigma^2 = \frac{\sum_{k=1}^n (x_k-\mu)^2}{n-1} $$

where
- $k$ is the sample index
- $n$ is the number of samples
- $x_k$ is the $k_{th}$ sample
- $\mu$ is the distribution mean

### Distribution Standard Deviation

The distribution standard deviation is the square root of the distribution variance. In other words, the standard 
deviation is the RMS value of all offsets between the distribution data and the distribution mean. The distribution 
standard deviation is defined as:

$$ \sigma = \sqrt{\sigma^2} $$

where
- $\sigma^2$ is the distribution variance

## Deviation Coverage

| x  | Covered population |
|:---:|:------------------:|
| 1σ | 68.269%            |
| 2σ | 95.450%            |
| 3σ | 99.730%            |
| 4σ | 99.994%            |
| 5σ | 99.99994%          |
| 6σ | 99.9999998%        |


## Combination of Distributions

The two methods of combining distributions are through addition and multiplication. Subtraction is the reverse of 
addition, and division is the reverse of multiplication.

### Addition of Two Independant Gaussian Distributions

Assuming that the two distributions (random variables) are independant, the addition of these two is achieved through 
the convolution of one with the other. The convolution of two Gaussian distributions results in a Gaussian distribution. Consider the two following Gaussian distributions:

$$ N(\mu_1,\sigma_1^2), N(\mu_2,\sigma_2^2) $$

The new Gaussian distribution created by the convolution of these two Gaussian distribution is defined as:

$$ N(\mu_c,\sigma_c^2) $$

where
- $\mu_c = \mu_1 + \mu_2$
- $\sigma_c^2 = \sigma_1^2 + \sigma_2^2$

### Multiplication of Two Independant Gaussian Distributions

Assuming that the two distributions (random variables) are independant. The multiplication of these two distributions does not result in a Gaussian distribution.

Warning: This section is not complete.
{: .notice}

#### Approximated Multiplication

Although the multiplication of two Gaussian distributions does not results in a Gaussian distribution, it may be convinient to approximate the result as one. If it is acceptable, consider the two following Gaussian distributions:

$$ N(\mu_1,\sigma_1^2), N(\mu_2,\sigma_2^2) $$

The approximated Gaussian distribution created by the multiplication of these two Gaussian distribution is defined as:

$$ N(\mu_m,\sigma_m^2) $$

where
- $\mu_m = \frac{\mu_1 \sigma_2^2 + \mu_2 \sigma_1^2}{\sigma_1^2 + \sigma_2^2}$
- $\sigma_m^2 = \frac{1}{\frac{1}{\sigma_1^2} + \frac{1}{\sigma_2^2}}$
