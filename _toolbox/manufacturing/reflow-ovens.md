---
layout: toolbox
sidebar:
    nav: "toolbox"
title: Commercial Prototyping Reflow Ovens
---

This is a list of reflow ovens meant for prototyping that I have found when searching for some. The only ones that I have experience using are the Vapourflow and the Controleo.

## IR Reflow Ovens
These ovens use infrared heaters to create and transfer heat.

- [LPKF](https://www.lpkf.com/en/industries-technologies/research-in-house-pcb-prototyping/products/lpkf-protoflow-s4) (Starting at approx 7000 USD)
- [DDM Novastar](https://www.ddmnovastar.com/reflow-ovens) (Starting at 4950 USD)
- [Fritsch](https://www.fritsch-smt.de/en/reflow/batch) (Unknown price)
- [Autotronik](https://www.autotronik-smt.de/en/products/reflowoefen/batch-ofen-bt301) (Unknown price)
- [Manncorp](https://www.manncorp.com/collections/benchtop-smt-reflow-ovens/products/batch-smt-reflow-oven-benchtop-mc301) (Unknown price)

## Vapour Phase Reflow Ovens
These ovens use a "reflow fluid" such as Galden LS230 to transfer heat.

- [Vapourflow](https://www.vaporflow.eu/) (Starting at approx 1500 EUR)
- [IMDES](https://www.imdes.de/produkt-kategorie/kondensations-reflow-loetanlagen-en/?lang=en) (Starting at approx 1600 EUR)
- [IBL-Tech](https://ibl-tech.com/en/batch-soldering/#minilab) (Starting at approx 10 000 EUR)
- [ASSCON](https://www.asscon.de/en/products/vp310/) (Starting at approx 1600 EUR)
- [PCB Arts](https://shop.pcb-arts.com/products/vapor-phase-one) (Starting at approx 5000 EUR)

## Modified Toaster Ovens
These ovens are more common in the hobby space than in the commercial space. These ovens typically involve buying a 3rd-party toaster oven or air frier and modifying it.

- [Controleo](https://www.whizoo.com/) (Starting at 139 USD)
- [picoReflow](https://apollo.open-resource.org/mission:resources:picoreflow) (Entirely DIY)
- [Reflow Master Pro](https://reflowmasterpro.com/) (Starting at 89 USD)
