---
layout: toolbox
sidebar:
    nav: "toolbox"
title: Reflow Soldering
mathjax: true
---

## Double-sided Assembly

Assembling a PCB with surface mount components on both sides of the board can be difficult when reflow soldering. One 
side of the board must be facing down, and so the components mounted to the bottom-facing side of the board may fall 
once their solder joints melt. Unless all components on the bottom-facing side of the PCB are glued to the PCB, 
components may only be reflow soldered when their side of the PCB is facing upwards. This leads to the typical 
requirement that double-sided PCBAs must go through two separate reflow cycles.

Fortunately, the surface tension of molten solder is significant. If a component has a total wetted pad surface area 
great enough to counteract the effect of gravity (and potential mechanical shock imparted when travelling through a 
mechanised reflow oven) on the mass of the component, the component will not fall. As a general rule, it may be assumed 
that components which meet the following metric will not fall:

$$ {46 \frac{mg}{mm^2} > \frac{M_{component}}{A_{contact}}} $$

But what if some components do not meet the aforementioned metric, and do not provide enough pad area? The general 
strategy is to try to have all of these heavy components on one side of the PCB, along with temperature-sensitive 
components would preferably only be reflowed once (such as some precision analog parts and non-volatile memories). This 
side will be referenced to as the "primary side". The secondary side must solely be populated with components that are 
neither too heavy nor temperature-sensitive. The following procedure may be followed:

1. Apply solder paste to the secondary side.
2. Place components on the secondary side.
3. Reflow the partially-populated PCBA with the secondary side facing upwards.
4. Clean the partially-populated PCBA once it has cooled. This step is optional depending on the flux used. Flux will 
generally become harder to remove with every reflow.
5. Apply solder paste to the primary side. The PCB will likely need to be elevated off of a surface due to the uneven 
height of components now on the bottom of the PCB.
6. Place components on the primary side.
7. Reflow the PCBA with the primary side facing upwards. The PCB will need to be elevated, as any pressure on the 
secondary side components will result in a crooked or even desoldered component.
8. Clean the PCBA once it has cooled.

## Package Suitability for Bottom-Mounting

The following tables list the component packages that should be able to be bottom-mounted without additional adhesives 
needing to be applied or other procedural requirements.

### Capacitors

| Package | Typical mass (mg) | Typical contact area (mm2) | Typical ratio (mg/mm2) | Acceptability |
|:-------:|:-----------------:|:--------------------------:|:----------------------:|:-------------:|
| 0201    | 0.57              |                            |                        | Yes           |
| 0402    | 3.10              |                            |                        | Yes           |
| 0603    | 9.10              |                            |                        | Yes           |
| 0805    | 23.0              |                            |                        | Yes           |
| 1206    | 64.0              |                            |                        | Yes           |
| 1210    | 110               |                            |                        | Yes           |

### Resistors

| Package | Typical mass (mg) | Typical contact area (mm2) | Typical ratio (mg/mm2) | Acceptability |
|:-------:|:-----------------:|:--------------------------:|:----------------------:|:-------------:|
| 0201    | 0.17              |                            |                        | Yes           |
| 0402    | 0.65              |                            |                        | Yes           |
| 0603    | 2.00              |                            |                        | Yes           |
| 0805    | 5.5               |                            |                        | Yes           |
| 1206    | 10.0              |                            |                        | Yes           |
| 1210    | 16.0              |                            |                        | Yes           |
| 2010    | 25.5              |                            |                        | Yes           |

