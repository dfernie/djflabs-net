---
layout: toolbox
sidebar:
    nav: "toolbox"
title: NASA EEE-INST-002 Wire Sizing
---

## Determining the Current Rating

Warning: This section is not complete.
{: .notice}

## Reference Table

Conditions are single wire, 70°C ambient temperature, hard vacuum of 10-6 torr, 200°C rated wire:
- For 150°C wire, use 80% of values shown in the table
- For 135°C wire, use 70% of values shown in the table
- For 260°C wire, 115% of values shown in the table may be used

| Wire Size (AWG) | Single-stranded Current (A) | Multi-stranded Current |
|:---------------:|:---------------------------:|:----------------------:|
| 30              | 1.3                         | 0.7                    |
| 28              | 1.8                         | 1.0                    |
| 26              | 2.5                         | 1.4                    |
| 24              | 3.3                         | 2.0                    |
| 22              | 4.5                         | 2.5                    |
| 20              | 6.5                         | 3.7                    |
| 18              | 9.2                         | 5.0                    |
| 16              | 13                          | 6.5                    |
| 14              | 19                          | 8.5                    |
| 12              | 25                          | 11                     |
| 10              | 33                          | 16                     |
| 8               | 44                          | 23                     |
| 6               | 60                          | 30                     |
| 4               | 81                          | 40                     |
| 2               | 108                         | 50                     |
| 0               | 147                         | 75                     |
| 00              | 169                         | 87                     |


