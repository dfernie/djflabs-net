---
layout: toolbox
sidebar:
    nav: "toolbox"
title: Electrical Wire Insulation Materials
---

## Insulation Materials

| Material              | Temperature Range         | Thermal Resistance \(K \• m / W\) | Emissivity | Dielectric Constant | Loss Tangent |
|:---------------------:|:-------------------------:|:---------------------------------:|:----------:|:-------------------:|:------------:|
| ETFE \(Tefzel\)       | \-200°C to 150°C or 200°C | 5\.56                             | 0\.89      | 2\.6                | 4\.1e\-3     |
| Polyalkene            |
| Polyimide \(Kapton\)  | \-269°C to 400°C          | 1\.65                             | 0\.95      | 5\.74               | 1\.3e\-2     |
| PTFE \(Teflon\) & FEP | \-90°C to 200°C or 260°C  | 3\.33                             | 0\.92      | 2\.1                | 1e\-4        |
| PVC                   | \-55°C to 105°C           | 6\.37                             | 0\.92      | 3\.13               | 4e\-2        |
| Silicone Rubber       | \-90°C to 200°C           | 0\.63                             | 0\.68      | 2\.6                |

## NASA Wire Insulation Selection Guidelines

The following table lists the wire insulation material tradeoffs compiled by the NASA parts selection list. PVC is not included in the list due to its general poor characteristics, and explicit banning for space projects.

| Material | Advantages | Disadvantages |
|:---------|:-----------|:--------------|
| FEP & PTFE | • Excellent high temperature properties.  PTFE Teflon is preferred for solder applications.  FEP is preferred for jacket material.<br> • Non-flammable.<br> • Good outgassing characteristics.<br> • Most flexible of all insulations.<br> • Good weatherability, resists moisture absorption and atomic oxygen erosion. | • Susceptible to cold flow when stressed (bent) over tight radius or when laced too tightly.<br> • Degraded by solar radiation above 5 x 10<sup>5</sup> rads.<br> • FEP has poor cut through resistance.<br> • Heaviest insulation. |
| ETFE | • Withstands physical abuse during and after installation.<br> • Good high and low temperature properties.<br> • High flex life.<br> • Good outgassing characteristics.<br> • Fair cold flow properties. | • Some ETFE insulations fail flammability in a 30% oxygen environment.<br> • Insulation tends to soften at high temperature.<br> • Degraded by gamma radiation above 10<sup>6</sup> rads |
| Crosslinked ETFE | • Higher strength than normal ETFE.<br> • Resistant to cold flow and abrasion.<br> • More resistant to radiation effects (up to 5 x 10<sup>7</sup> rads).<br> • Higher maximum temperature than normal ETFE (150°C for tin plating, 200°C for silver plating).<br> • Good outgassing characteristics. | • Some ETFE insulations fail flammability in a 30% oxygen environment.<br> • Less flexible than extruded ETFE.<br> • More difficult to work with than PTFE. |
| Polyimide | • Lightest weight wire insulation material. Commonly used with FEP or PTFE Teflon to form layered insulation tapes.<br> • Excellent physical thermal and electric properties. Excellent cut-through resistance and cold flow resistance.<br> • Excellent radiation resistance (up to 5 x 10<sup>9</sup> rads).<br> • Good outgassing characteristics. | • Inflexibility - difficult to strip.<br> • Absorbs moisture. Degraded by atomic oxygen. Poor weatherability.<br> • Prone to wet-arc and dry-arc tracking from abrasions and cuts.<br> • More difficult to flex.<br> • Not stable to ultraviolet radiation. |
| Crosslinked Polyalkene | • Dual extrusion which is fused by sintering. Combines excellent abrasion and cut through resistance of Polyvinylidene Fluoride (PVDF, PVF2-Penwalt Corp. Kynar) with Polyolefin for greater flexibility and improved heat resistance. Polyalkene is used mainly as a primary insulation under an outer jacket such as crosslinked ETFE or crosslinked PVDF/PVF2.<br> • High dielectric constant, used in high voltage applications.<br> • PVDF has good radiation resistance (up to 10<sup>8</sup> rads).<br> • More resistant to cold flow.<br> • Good outgassing characteristics. | • Lower maximum conductor temperature rating (135°C for GSFC S-311-P-13, 150°C for MIL-W-81044).<br> • Reduced flexibility. |
| Silicone Rubber | • Excellent flexibility at low temperatures.<br> • Excellent high voltage corona resistance.<br> • Good radiation resistance (up to 10<sup>8</sup> rads).<br> • Good cold flow resistance. | • Poor cut through resistance, mechanical toughness, and fluid resistance.<br> • Must be processed for outgassing control.<br> • Flammable.<br> • No standard silicon rubber insulated wire or cable. |