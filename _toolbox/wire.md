---
layout: toolbox
sidebar:
    nav: "toolbox"
title: Electrical Wire
mathjax: true
---

## Current Limits
The current rating of a wire is purely a thermal problem; a specific wire will exhibit a temperature rise above ambient 
when a current is passed through it. There are two important points at which the wire temperature becomes important:

1. When the wire is accessible to a person, a wire temperature above approximately 50°C might be painful to touch.
2. When the wire temperature is high enough, it can soften or melt the wire insulation, potentially leading to a 
    situation where conductors may short together, or a surface may become electrified. In the worst case, wire 
    insulation could catch on fire.

[IEC 60287](iec60287) is the usual standard for determining the current carrying capacity of an electrical 
wire. For a more concervative estimate (or for vacuum applications), [NASA EEE-INST-002](eee-inst-002) or 
[ECSS-Q-ST-30-11C](ecss-q-st-30-11c) may be used.

## Wire Sizes

| Wire Size \(AWG\) | Wire Size \(mm2\) | Single\-strand Diameter \(mm\) | Multi\-strand Diameter \(mm\) | Resistance \(mΩ/m\) |
|:-----------------:|:-----------------:|:------------------------------:|:-----------------------------:|:-------------------:|
| 30                | 0\.051            | 0\.254                         | 0\.287                        | 338\.6              |
| 28                | 0\.081            | 0\.32                          | 0\.361                        | 212\.9              |
| 26                | 0\.129            | 0\.404                         | 0\.287                        | 133\.9              |
| 24                | 0\.205            | 0\.511                         | 0\.287                        | 84\.29              |
| 22                | 0\.326            | 0\.643                         | 0\.287                        | 52\.65              |
| 20                | 0\.518            | 0\.813                         | 0\.287                        | 33\.3               |
| 18                | 0\.823            | 1\.024                         | 1\.156                        | 20\.95              |
| 16                | 1\.309            | 1\.29                          | 1\.458                        | 13\.18              |
| 14                | 2\.081            | 1\.628                         | 1\.839                        | 8\.284              |
| 12                | 3\.309            | 2\.052                         | 2\.319                        | 5\.21               |
| 10                | 5\.261            | 2\.588                         | 2\.924                        | 3\.277              |
| 8                 | 8\.366            | 3\.264                         | 3\.688                        | 2\.061              |
| 6                 | 13\.3             | 4\.115                         | 4\.651                        | 1\.296              |
| 4                 | 21\.15            | 5\.189                         | 5\.865                        | 0\.815              |
| 2                 | 33\.63            | 6\.543                         | 7\.394                        | 0\.513              |
| 0                 | 53\.48            | 8\.253                         | 9\.324                        | 0\.322              |
| 00                | 67\.43            | 9\.266                         | 10\.47                        | 0\.256              |

## AWG - Metric Conversion

Dimensions of AWG electrical wire are defined by the ASTM B258 standard. Dimensions of metric electrical wire are 
defined by the IEC 60228 standard. The following equation defines the equivalence between the two standards:

$$ metric (mm^2) = 0.012668 \cdot 92^{[\frac{36-AWG}{19.5}]} $$
