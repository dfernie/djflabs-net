---
layout: toolbox
sidebar:
    nav: "toolbox"
title: E Series of Preferred Values
---

From Wikipedia: The E series of preferred numbers were chosen such that when a component is manufactured it will end up 
in a range of roughly equally spaced values on a logarithmic scale. Each E series subdivides each decade magnitude into 
steps of 3, 6, 12, 24, 48, 96, 192 values. Subdivisions of E3 to E192 ensure the maximum error will be divided in the 
order of 40%, 20%, 10%, 5%, 2%, 1%, 0.5%. Also, the E192 series is used for 0.25% and 0.1% tolerance resistors.

Historically, the E series is split into two major groupings:

* E3, E6, E12, E24 -- E3, E6, E12 are subsets of E24. Values in this group are rounded to 1 trailing digit.
* E48, E96, E192 -- E48 and E96 are subsets of E192. Values in this group are rounded to 2 trailing digits.

Since the electronic component industry established component values before standards discussions in the late-1940s, 
they decided that it wasn't practical to change the former established values. These older values were used to create 
the E6, E12, E24 series standard that was accepted in Paris in 1950 then published as IEC 63 in 1952. Eight of the E24 
values do not match the established formula.